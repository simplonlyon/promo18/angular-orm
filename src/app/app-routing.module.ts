import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DogListComponent } from './dog-list/dog-list.component';
import { SingleDogComponent } from './single-dog/single-dog.component';

const routes: Routes = [
  {path:'', component:DogListComponent},
  {path:'dog/:id', component: SingleDogComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
