import { Component, Input, OnInit } from '@angular/core';
import { Contest } from '../entities';

@Component({
  selector: 'app-contest-list',
  templateUrl: './contest-list.component.html',
  styleUrls: ['./contest-list.component.css']
})
export class ContestListComponent implements OnInit {
  @Input()
  contests:Contest[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
