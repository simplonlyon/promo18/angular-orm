import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { DogService } from '../dog.service';
import { Contest, Dog } from '../entities';

@Component({
  selector: 'app-single-dog',
  templateUrl: './single-dog.component.html',
  styleUrls: ['./single-dog.component.css']
})
export class SingleDogComponent implements OnInit {
  dog?:Dog;
  contests:Contest[] = [];

  constructor(private dogService:DogService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.dogService.getOne(params['id']))
    ).subscribe(data => this.dog = data);
    
    this.route.params.pipe(
      switchMap(params => this.dogService.getContest(params['id']))
    ).subscribe(data => this.contests = data);
  }

}
