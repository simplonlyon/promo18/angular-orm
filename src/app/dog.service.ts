import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Contest, Dog, Page } from './entities';

@Injectable({
  providedIn: 'root'
})
export class DogService {

  constructor(private http:HttpClient) { }

  getAll(page = 0, pageSize = 10) {
    //Fera une requête vers http://localhost:8080/api/dog?page=0&pageSize=10 par défaut
    return this.http.get<Page<Dog>>(environment.apiUrl+ '/api/dog', {
      params:{page,pageSize}
    });
  }

  getOne(id:number) {
    return this.http.get<Dog>(environment.apiUrl+ '/api/dog/'+id);
  }

  getContest(id:number) {
    return this.http.get<Contest[]>(environment.apiUrl+ '/api/dog/'+id+'/contest');

  }
}
