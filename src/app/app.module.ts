import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DogListComponent } from './dog-list/dog-list.component';
import { FormsModule } from '@angular/forms';
import { SingleDogComponent } from './single-dog/single-dog.component';
import { ContestListComponent } from './contest-list/contest-list.component';

@NgModule({
  declarations: [
    AppComponent,
    DogListComponent,
    SingleDogComponent,
    ContestListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
